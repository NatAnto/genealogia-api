<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValutazioneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valutazione', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->date('data valutazione');
            $table->integer('sviluppo_primaverile');
            $table->integer('sviluppo_estivo');
            $table->integer('sviluppo_autunnale');
            $table->integer('aggressivita');
            $table->integer('docilita');
            $table->integer('tendenza_alla_sciamatura');
            $table->integer('produttivita_primaverile');
            $table->integer('produttivita_estiva');
            $table->integer('varroe_primaverili');
            $table->integer('varroe_estive');
            $table->integer('varroe_autunnalli');
            $table->integer('igienicita');
            $table->integer('paralisi_cronica');
            $table->char('dimensione_colonia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valutazione');
    }
}
