<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReginaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regina', function (Blueprint $table) {
            $table->id();
            $table->char('nome');
            $table->date('nascita');
            $table->date('morte');
            $table->double('dimensione');
            $table->char('modalita_nascita');
            $table->char('colore');
            $table->char('caratteristiche');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regina');
    }
}
